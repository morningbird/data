<?php
namespace morningbird\data;

use Yii;
use yii\db\Query;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class ExcelEngine {



    /**
     * @param string $dest destination file
     * @param string $tableName Table name
     * @param mixed $attributes list attribute in array
     */
    public function exportToExcel($tableName, $attributes, $dest = 'php://output')
    {
        //buat query object
        $query = new Query;
        $query->select($attributes)
              ->from($tableName);

        //panggil export excel query
        $this->exportToExcelFromQuery($dest, $query);
    }

    /**
     * Convert query to excel
     * @param string $dest destination file
     * @param mixed $query yii\db\Query object or array of yii\db\Query object
     */
    public function exportToExcelFromQuery($querys, $dest = 'php://output')
    {
        $spreadsheet = new Spreadsheet();
        //hapus sheet default
        $spreadsheet->removeSheetByIndex(0);

        $ctrSheet = 0;

        //jika bukan array, buat jadi array dulu
        if(!is_array($querys))
        {
            $querys = [$querys];
        }

        foreach($querys as $query)
        {
            //tambahkan sheet sesuai dengan nama table
            $myWorkSheet = new Worksheet($spreadsheet, $query->from[0]);
            $spreadsheet->addSheet($myWorkSheet, $ctrSheet);
            $sheet = $spreadsheet->getSheet($ctrSheet);
            
            $attributes = $query->select;
            //row pertama untuk header/ nama attribute
            $columnIndex = 1;
            foreach($attributes as $attr) {
                $attr = $this->getAttribute($attr);
                $sheet->setCellValueByColumnAndRow($columnIndex, 1, $attr);
                $columnIndex++;
            }

            //row berikutnya untuk data
            $rowIndex = 2;
            foreach($query->each() as $row) {
                $columnIndex = 1;
                foreach($attributes as $attr) {
                    $attr = $this->getAttribute($attr);
                    $sheet->setCellValueByColumnAndRow($columnIndex, $rowIndex, $row[$attr]);
                    $columnIndex++;
                }
                //increment row index
                $rowIndex++;
            }
            $ctrSheet++;
        }

        $writer = new Xlsx($spreadsheet);
        if($dest==='php://output')
        {
            header("Content-Type: application/vnd.ms-excel; charset=utf-8");
            header("Content-Disposition: attachment; filename=".date('YmdHi').".xls");
        }
        else {
            $dest = Yii::getAlias($dest);
        }
        $writer->save($dest);
    }

    private function getAttribute($attr) {
        $s = $attr;
        $posDot = strpos($attr, '.');
        if ($posDot > -1) {
            $s = substr($attr, $posDot + 1);
        }
        return $s;
    }

    private function updateField($tableName, $pk, $fields, $data)
    {
        // mulai update
        foreach($data as $row)
        {
            $row = array_values($row);
            $pkValue = $row[0];
            //jika $pkValue empty, maka skip saja
            if(empty($pkValue))
            {
                continue;
            }
            $updateField = [];
            for($i=1;$i<count($fields);$i++)
            {
                $fieldName = $fields[$i];
                $updateField[$fieldName] = $row[$i];
            }
            Yii::$app->db->createCommand()
                ->update($tableName, $updateField, [$pk => $pkValue])
                ->execute();
        }
    }

    private function numberToExcelColumn($number)
    {
        return chr($number+65);
    }

    public function importFromExcel($filePath)
    {
        // $client = new \Redis();
        // $client->connect('127.0.0.1', 6379);
        // $pool = new \Cache\Adapter\Redis\RedisCachePool($client);
        // $simpleCache = new \Cache\Bridge\SimpleCache\SimpleCacheBridge($pool);
        // \PhpOffice\PhpSpreadsheet\Settings::setCache($simpleCache);
        $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader("Xlsx");
        $spreadsheet = $reader->load($filePath);

        $trans = Yii::$app->db->beginTransaction();
        //ambil daftar nama sheet
        try {
            foreach($spreadsheet->getSheetNames() as $name)
            {
                $sheet = $spreadsheet->getSheetByName($name);
                // ambil nama field primary key
                $maxCol = $sheet->getHighestColumn();
                $maxRow = $sheet->getHighestRow();
                $range = 'A1:' . $maxCol . '1';        
                $data = array_values($sheet->rangeToArray($range, null, true, true, true));
                $pk = $data[0]['A'];
                $fields = [];
                //ambil daftar nama field yang ada
                $row = array_values($data[0]);
                $ctr = -1;
                for($i=0;$i<count($row);$i++)
                {
                    if(empty($row[$i]))
                    {
                        continue;
                    }
                    $fields[] = $row[$i];
                    $ctr++;
                }
                //menghitung ulang max column karena phpspreadsheet supaya hanya yang perlu saja yang di load
                $maxCol = $this->numberToExcelColumn($ctr);
                // ambil datanya per 100 baris
                $startRow = 2;
                $steps = 100;
                $endRow = $startRow + $steps;
                do
                {
                    $range = "A{$startRow}:{$maxCol}{$endRow}";
                    $data = array_values($sheet->rangeToArray($range, null, true, true, true));
                    $this->updateField($name, $pk, $fields, $data);
                    $startRow += $steps + 1;
                    $endRow = $startRow + $steps;
                    if($endRow>$maxRow)
                    {
                        $endRow = $maxRow;
                    }
                }while($maxRow>=$startRow);    
            }
            $trans->commit();
        } catch(\Exception $exc)
        {
            $trans->rollback();
            echo $exc->getMessage();
        }
    }
}